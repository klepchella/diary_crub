import csv
import logging
from time import sleep
from typing import Optional

import requests
from bs4 import BeautifulSoup

from pagionation import Page

DIARY_LOGIN = "https://diary.ru/user/login"
DIARY_LINK = "https://diary.ru/~"


logger = logging.getLogger(__name__)


class Crab:
    def __init__(self, login: str = "", password: str = "", *args, **kwargs):
        self.posts: list = []
        self.posts_ids = set()
        self.content = None

        self._validate_params(login, password, args, kwargs)
        diary_link, response = self.login()
        self.parsing_all_data(diary_link, response)

        self.logout()

    def _validate_params(self, login, password, *args, **kwargs):
        self._login = login
        self._password = password

    def login(self):
        diary_link = f"{DIARY_LINK}{self._login}"
        response = requests.request(
            "GET",
            diary_link
            # auth=(self._login, self._password)
        )
        assert response.status_code == 200
        return diary_link, response

    def logout(self):
        ...

    def get_next_page(self):
        try:
            page = self.page.get_next_link()
        except StopIteration:
            return
        return page

    def grab_records(self, posts):
        for post in posts:
            text_post = self.get_record(post)
            if text_post:
                self.posts.append(text_post)

    def parsing_all_data(self, link, response):
        next_link = link
        while next_link:
            try:
                self.get_request(next_link, response.cookies)
            except Exception as exc:
                raise Exception from exc
            self.page = self.create_pagination(link, next_link)
            posts = self.parse_page()

            self.grab_records(posts)
            try:
                next_link = self.get_next_page()
            except Exception as exc:
                logging.error(f"something went wrong: {exc}", exc_info=True)
            else:
                if not next_link:
                    break
                sleep(5)

        self.write_to_file()

    def create_pagination(self, root_link, current_link):
        page = Page(self.content.find(class_="pagination"), root_link, current_link)
        return page

    def get_request(self, link, cookies):
        response = requests.request("GET", link, cookies=cookies)
        assert response.status_code == 200
        self.content = BeautifulSoup(response.text)

    def parse_page(self):
        posts = self.content.find_all(class_="singlePost")
        return posts

    def get_record(self, post) -> Optional[dict]:
        """
        get text from record at html-format
        :return:
        """
        post_id = self.get_post_id(post)
        if self.check_post_id(post_id):
            return
        else:
            self.posts_ids.add(post_id)

        created_time, title = self.get_time_created_and_title(post)

        record_data = {
            "id": post_id,
            "date_created": self.get_records_created_date(post),
            "time_created": created_time,
            "title": title,
            "text": self.get_text(post),
            "tags": self.get_tags(post),
        }
        return record_data

    def get_text_and_tags(self, post):
        text_with_tags = post.find(class_="postInner")
        text = self.get_text(text_with_tags)

        tags_html = text_with_tags.find(class_="tags atTag")
        tags = self.get_tags(tags_html)

        return text, tags

    def get_text(self, text_html):
        text = text_html.find(style="min-height:40px") or False
        return text.renderContents().decode() if text else ""

    def get_tags(self, tags):
        tags_list = []
        tags_html = tags.find_all(class_="tags atTag")
        for tag in tags_html:
            tags_list.extend([x.text for x in tag.find_all("a")])
        return tags_list

    def check_post_id(self, post_id):
        if post_id in self.posts_ids:
            return True
        else:
            return False

    def get_records_created_date(self, post):
        created_date = post.find(class_="countSecondDate postDate uline")
        text_date = created_date.text.strip() if created_date else ""
        return text_date

    def get_time_created_and_title(self, post):
        created_time = post.find(class_="postTitle header")
        data = list(filter(lambda x: x, created_time.text.split("\n")))

        created_time = data[0] if data else ""
        title = self.get_title(data)

        return created_time, title

    def get_title(self, data):
        if not data:
            title = ""
        elif len(data) == 1:
            title = ""
        else:
            title = data[1]
        return title

    def get_post_id(self, post):
        id = post.attrs.get("id") or 0
        return id

    def write_to_file(self):
        with open("records.csv", "a+", newline="") as writer:
            dict_write = csv.DictWriter(
                writer,
                fieldnames=[
                    "id",
                    "date_created",
                    "time_created",
                    "title",
                    "text",
                    "tags",
                ],
            )
            dict_write.writerows(self.posts)
