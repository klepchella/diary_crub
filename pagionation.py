class Page:
    def __init__(self, content=None, root_link: str = "", current_link: str = ""):
        self.content = content
        self.root_link = root_link
        self.current_link = current_link
        self.current_page = self.get_current_page()
        self.next_page = self.get_next_page()

    def get_current_page(self):
        return self.content.find(class_="active")

    def get_next_page(self):
        current_page = self.current_page.nextSibling
        if current_page:
            return self.current_page.nextSibling

    def get_next_link(self):
        next_page = self.next_page
        if next_page:
            pagination_link = self.next_page["href"].split("?")[-1]
            return f"{self.root_link}?{pagination_link}/" if pagination_link else None
        raise StopIteration
